DDWW Handbook

1.  Overview of DDWW

    a.  Mission Statement

    b.  Organizational structure

    c.  Affiliate relationship with DDWW

2.  Setting up a club

    a.  Club name

    b.  Selecting a board

    c.  Setting up the Banking

    d.  Building a membership base

    e.  Club Logo

    f.  Website

    g.  Social media

    h.  Affiliate agreement

    i.  Tax status

    j.  Merchandise

    k.  Bylaws

3.  Rules, Policies, & Procedures

    a.  Rules Committee

        i.  Meeting Schedule

        ii. Selecting attendees

        iii. Roles of Rules Committee members

        iv. Confidentiality of proceedings at Rules

    b.  Current Rules, Policies, Procedures

4.  Practices

    a.  Finding a location

    b.  Frequency/duration

    c.  Committee to organize

    d.  Insurance requirements

5.  Events

    a.  Gaining events

        i.  How to find events

        ii. Pricing structure

    b.  Pre-event

        i.  Contract

        ii. Insurance

        iii. Reserving rig with DDWW

        iv. Determining wave schedules for event

        v.  Promotion

        vi. Inspecting a potential site

        vii. Determining prize structure

        viii. Ordering medals/ribbons

        ix. Finding sponsors

        x.  Organizing volunteers

        xi. Certified judges

        xii. Announcer notebook

    c.  Weekend of event

        i.  Set up

        ii. Tear down

        iii. Pool requirements

        iv. Work schedule for volunteers

        v.  Merchandise

        vi. Relationship with event host

        vii. First aid kit

        viii. Rules infractions

    d.  Post-event

        i.  Follow up with event host

        ii. Follow up with DDWW

        iii. Follow up with club members

6.  Club meetings

    a.  Business meetings

    b.  Social events

7.  Miscellaneous Club Activities

    a.  Voucher system

    b.  Community Outreach events

> ***Overview of DDWW***

1.  Mission Statement:

    ***DockDogs® Mission Statement***

    ***DockDogs® maintains zero tolerance for bullying, harassment,
    and/or unethical treatment of spectators, competitors, employees,
    and canines. Everyone involved follows the rules and policies and
    ethical guidelines which are the foundation of
    DockDogs® Worldwide.***

2.  Organizational structure

    DockDogs® Worldwide 330-241-4975

    Grant Reeves CEO - grant.reeves@dockdogs.com

    Teresa Reeves – General Manager <Teresa.reeves@dockdogs.com>

    Vicki Tighe – Director of Affiliates <vicki.tighe@dockdogs.com>
    239-464-1859

    Betsy Taylor – S/C-S/W Regional Rep <betsy.taylor@dockdogs.com>
    337-371-8551

    Joan Gunby – N/E Regional Rep <joan.gunby@dockdogs.com> 410-903-7137

    Linda Ruiz – N/C – N/W Regional Rep <lindaruiz@dockdogs.com>
    763-234-9538

    Lisa Hudgens- S/E Regional Rep <lisa.hudgens@dockdogs.com>
    865-406-5994

    Brian King – IT brian.king@dockdogs.com

    Theresa Bufilino – Accounting <theresa.bufalino@dockdogs.com>

    Linda Torson – Administration, Elections <dd_admin@dockdogs.com>

    Sam Bruno – Accounting <sam.bruno@dockdogs.com>

    Brian Sharenow – Events <brian.sharenow@dockdogs.com>

    Sean Swearinger – Event Crew Manager <sean.swearinger@dockdogs.com>

3.  Affiliate relationship with DDWW

4.  **History of DockDogs**

    a.  1999 Established with canine aquatics competition demonstration
        at ESPN Great Outdoor Games. DockDogs personel would knock on
        doors, call folks asking them if they had dogs that wanted to
        jump in the water; asked if they wanted to compete in ESPN great
        outdoor games demo.

    b.  2000 The first Great Outdoor Games competition 12 dogs competed.
        The finals format was 12 dogs, to 6 dogs, to 4 dogs; each round
        of <span id="6" class="anchor"></span>competition had 4 jumps.

5.  **Historical Milestones in Dockdogs**

2014

-   Mike Ziler & Gunner Boy - 2014 DockDogs Hall of Fame Inductee

    Dan Jacobs & Missy - 2014 DockDogs Hall of Fame Inductee

    Joey Spurlock & Brock - 2014 DockDogs Hall of Fame Inductee

    20,000 LIKES

2013

-   DockDogs Soars To NEW High Flyer Heights With Over 12,000 likes!

    First Radio DockDogs Broadcast

    2012

<!-- -->

-   2012 DockDogs Hall of Fame Inductees - Sean McCarthy & Jordan

    2012 DockDogs Hall of Fame Inductees - Jay Harris & Sir Harley

    DockDogs Australia Launches First Club - Gold Coast DockDogs

    8,000 likes on Facebook

    New DockDogs Website

    DockDogs Launches "Latest From The Dock" blog

    Speed Retrieve Indoor World Record - Tom Dropik & Remi

    Big Air Indoor World Record - Tony Lampert & Baxter

    Big Air Indoor World Record - Tony Lampert & Baxter

    Big Air Indoor World Record - Tony Lampert & Baxter

    Launch of new ERS

    Becky Havlinek & Autumn - Hall of Fame Inductee

2011

-   Extreme Vertical Outdoor World Record - Dave Skoletsky & Yeager

    Tim Kline & Bella - 2011 Hall of Fame Inductee

    Kristi Baird & Henna - 2011 Hall of Fame Inductee

    Extreme Vertical Outdoor World Record - Dave Skoletsky & Yeager

    Extreme Vertical Outdoor World Record - Dave Skoletsky & Yeager

    Extreme Vertical Outdoor World Record - Dave Skoletsky & Yeager

    Iron Dog Outdoor World Record - Mike Chiasson & Tax

    Big Air Indoor World Record - Tony Lampert & Baxter

    Iron Dog Outdoor World Record -Dave Skoletsky & Yeager

    Iron Dog Indoor World Record - John Langdon & Remy

    2010

<!-- -->

-   Sue Barnes & Tanner - 2010 Hall of Fame Inductee

    Craig Haverstick & Henry - 2010 Hall of Fame Inductee

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Iron Dog Outdoor World Record - Tony Lampert & Baxter

    Iron Dog Outdoor World Record - Craig Haverstick & Henry

    Iron Dog Indoor World Record - Craig Haverstick & Henry

    Big Air Indoor World Record - Jon Langdon & Remy

2009

-   Melissa Ness & Stryker - 2009 Hall of Fame Inductee

    Dean Skillman & Wylie - 2009 Hall of Fame Inductee

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Iron Dog Outdoor World Record - Fred Hassen & Rex

    Iron Dog Outdoor World Record - Craig Haverstick & Henry

    Extreme Vertical Indoor World Record - Andy Mace & Brox

    Extreme Vertical Outdoor World Record - Andy Mace & Brox

    Big Air Indoor World Record - Rande Murphy & Quasi

    Iron Dog Indoor World Record - Sean McCarthy & Jordan

    Speed Retrieve Indoor World Record - Sean McCarthy & Jordan

    Iron Dog Indoor World Record - Lee Hall & Chopper

    Big Air Indoor World Record -Rande Murphy & Quasi

    2008

<!-- -->

-   Iron Dog Outdoor World Record - Dave Speed & Jumpin' George

    Becky Havlinek & Autumn - 2008 Hall of Fame Inductee

    Dave Breen & Blackjack - 2008 Hall of Fame Inductee

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Speed Retrieve Outdoor World Record - Les Lamparski & Lucy

    Speed Retrieve Outdoor World Record - Sean McCarthy & Jordan

    Speed Retrieve Indoor World Record - Dean Skillman & Wylie

    Speed Retrieve Outdoor World Record - Tim Kline & Bella

    Extreme Vertical Indoor World Record - Jeff Wells & Ares

    Extreme Vertical Indoor World Record - Jeff Wells & Ares

    Speed Retrieve Indoor World Record - Dean Skillman & Wylie

    Iron Dog Discipline Introduced

    2007

<!-- -->

-   Mike & Mary Jo Kline - Haley - Hall of Fame Inductee

    Chris Litwin & Kiki - Hall of Fame Inductee

    Extreme Vertical Outdoor World Record - Kevin Meese & Country

    Big Air Indoor World Record - Joann Chronister & Rebel

    Extreme Vertical Indoor World Record - Linda Lamparski & Lucy

    DockDogs Magazine 1st Issue

    Introduced Speed Retrieve Discipline

2006

-   Kevin Meese & Courtner - Hall of Fame Inductee

    Linda & Randy Hettich - Kadin - Hall of Fame Inductee

    Extreme Vertical Outdoor World Record - Kevin Meese & Country

    Big Air Indoor World Record - Mike Ziler & Gunner Boy

    Extreme Vertical Indoor World Record - Bryan Peyton & Doc

    Extreme Vertical Indoor World Record - Phil Weaver & Willow

    Extreme Vertical Outdoor World Record - Melissa Ness & Stryker

    Extreme Vertical Indoor World Record - Dean Skillman & Wylie

    Big Air Indoor World Record - Dean Skillman & Wylie

    Extreme Vertical Indoor World Record - Melissa Ness & Stryker

    ESPN discontinued Great Outdoor Games

    1st DockDogs World Championship

2005

-   Big Air Outdoor World Record - Kevin Meese & Country

    Extreme Vertical Outdoor World Record - Anna Borovich & Colby

    Angie Jones & Nestle - Hall of Fame Inductee

    Mike Jackson & Little Morgan - Hall of Fame Inductee

    Big Air Outdoor World Record - Kevin Meese & Country

    Big Air Outdoor World Record - Kevin Meese & Country

    Big Air Outdoor World Record - Kevin Meese & Country

    Big Air Outdoor World Record - Kevin Meese & Country

    Extreme Vertical Outdoor World Record - Angie Jones & Nestle

    Extreme Vertical Outdoor World Record - Mark Stuart & Hogan

    Extreme Vertical Outdoor World Record - Tom Dropik & Tucker

    Introduction of astroturf

    Extreme Vertical Introduced

    1st official Extreme Vertical Rig - Western Regional Championships

    Extreme Vertical & Tom Dropik

    Tom Dropik & Tucker Take 1st place in first ever EV competition at
    Western Regional Finals

    DockDogs Introduces Head to Head finals

2004

-   Tom Dropik & Tucker - 2004 Hall of Fame Inductee

    Beth Gutteridge & Heidi - Hall of Fame Inductee

    Big Air Indoor World Record - Donna Ames & Draco

    Regional Qualifiers Introduced

    ESPN Great Outdoor Games

    EV demonstration by Tom Dropik & Tucker

2003

-   First West Coast Event

    40 Foot Docks

2002

-   Big Air Outdoor World Record - Mike Jackson & Little Morgan

    Big Air Outdoor World Record - John Kline & Haley

    Big Air Indoor World Record - Mike Jackson & Little Morgan

    Introduction of pools / Dock Modifications

2001

-   Big Air Outdoor World Record - Mike Wallace & Jerry

    DockDogs Qualifiers Across Country

    Official DockDogs Competition in ESPN Great Outdoor Games

    New Finals Format

2000

-   Great Outdoor Games

1999

-   Founded on June 1, 1999

> ***Setting up a club***

1.  **Club name:** The selection of your club name should contain
    DockDogs and be relevant to your regional area. (ex: Puget Sound
    DockDogs, Beantown DockDogs, etc.)

2.  **Selecting a board:** The governing board should be comprised of
    the Past President with no voting rights, the President, Vice
    President, Secretary, Treasurer, and a number of Directors making
    the total Governing Board an odd number not to exceed seven (7)
    voting members. Offices other than the President/Treasurer may
    be combined. In the event a club does not have an odd number of
    governing board members, their regional representative or the
    Director of Affiliate Clubs will serve as the additional
    board member. Should we add requirements

3.  **Banking:** When choosing a Bank it is recommended to consider the
    following: Convenience, choose a branch bank. As the Governing Board
    changes the Treasurer will always have easy access. Cost, let the
    bank know the Club is non-profit, most of them will have a free
    account if you keep a small minimum balance. Check Signatures, it is
    suggested to have two (2) signatures. It is highly recommended the
    Club only have a Debit Card, not a Credit Card.

4.  Building a membership base

5.  **Club Logo**: Investigate local options for logo development. There
    are also graphic artists within the DockDogs community who have
    experience making logos for clubs and individual teams.

    a.  CesLes Creative Services

    b.  Theresa M. Cooper Photography

6.  **Website**: Once the club name is approved, DockDogs® will secure
    and purchase the domain name. DockDogs® offers free website hosting
    to our Affiliate Clubs, or you may use any 3rd party service to
    develop your website. Brian King <brian.king@dockdogs> is the IT
    Manger.When developing the club website, affiliate clubs must work
    with DockDogs Worldwide in setting up the domain name and website.
    Contact <brian.king@dockdogs.com> to begin this process.

7.  **Social media**: Social media is an excellent tool for spreading
    the word about your club, gaining new members, and creating interest
    in your social events, practices, and competitive events. DockDogs®
    will set up the Club’s Facebook pace. Please send Brian J. King,
    Vicki L. Tighe and your Regional Rep a Friend Request. The Club can
    then appoint Editors and Content Creators. When establishing a
    Facebook page, Brian King from DockDogs Worldwide must also be
    listed as an administrator in addition to your club webmaster.

8.  **Affiliate agreement:** Each club signs a yearly Club Affiliation
    Application & Contractual Agreement affiliate agreement with
    DockDogs Worldwide. This agreement delineates the responsibilities
    of both the affiliate club and DockDogs Worldwide. This agreement is
    signed and submitted to DockDogs Worldwide at the origination of the
    affiliate club and each year thereafter. If a club sends
    representatives to the annual Rules Committee Meeting, the agreement
    will be signed and submitted with the Affiliation fee at that time.
    Otherwise, it must be signed and submitted with the Affiliation fee
    prior to Dec. 31 of that year.

9.  **Tax status**: DockDogs Affiliate Clubs must be non-profit
    organizations according to the bylaws. There are several steps that
    must be followed in becoming an official non-profit organization.

    a.  File with your state as a non-profit organization

    b.  Apply for an EIN number with the IRS. You can do so easily
        online with the following link:
        <https://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/Apply-for-an-Employer-Identification-Number-%28EIN%29-Online>

    c.  You can take an additional step of becoming a 501(c) non-profit.
        DDWW Affiliate Clubs fit under two types of 501(c)s.

        i.  501c7: social club

            1.  The club must be organized for [*exempt purposes
                *](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Exempt-Purposes-Code-Section-501%28c%29%287%29).

            2.  Substantially all of its activities must further
                [*exempt purposes
                *](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Exempt-Purposes-Code-Section-501%28c%29%287%29)

            3.  If the club exceeds [*safe harbor
                guidelines*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Nonmember-Income-of-Tax-Exempt-Social-Clubs-Effect-on-Exempt-Status)
                for nonmember and investment income, the facts and
                circumstances must show that it is organized
                substantially for exempt purposes.

            4.  The club has *de minimis* income from
                [*nontraditional*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Traditional-and-Nontraditional-Business-Activities-of-Tax-Exempt-Social-Clubs)
                sources (*i.e.*, from investments or from activities
                that, if conducted with members, would further the
                club's tax-exempt purposes).

            5.  For a discussion of the effect of nonmember and
                "nontraditional" income on the tax-exempt status of
                social clubs under section 501(c)(7), see [*Nonmember
                Income*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Tax-Issues-for-Tax-Exempt-Social-Clubs).

            6.  The club must provide an opportunity for [*personal
                contact*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Social-Clubs-%E2%80%93-Requirements-for-Exemption-%E2%80%93-Personal-Contact-Required)
                among members, and [*membership must be
                limited*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Social-Clubs-%E2%80%93-Requirements-for-Exemption-%E2%80%93-Limited-Membership).

            7.  The club must be [*supported by
                membership*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Social-Clubs-%E2%80%93-Requirements-for-Exemption-%E2%80%93-Support-By-Membership-Dues)
                fees, dues, and assessments.

            8.  The organization’s net [*earnings may not
                inure*](http://www.irs.gov/Charities-&-Non-Profits/Other-Non-Profits/Social-Clubs-%E2%80%93-Requirements-for-Exemption-%E2%80%93-Inurement-Prohibited)
                to the benefit of any person having a personal and
                private interest in its activities.

            9.  The club's governing instrument may not contain a
                provision that provides for discrimination against any
                person on the basis of race, color, or religion.

            10. The club may not hold itself out as providing goods and
                services to the general public.

            11. You can apply as a 501(c)7 at:
                <http://www.irs.gov/pub/irs-pdf/k1024.pdf>

        ii. 501c3: *types of organizations under 501c3 (*charitable,
            religious, educational, scientific, literary, testing for
            public safety, fostering national or international amateur
            sports competition, and preventing cruelty to children
            or animals.)

            1.  To be tax-exempt under section 501(c)(3) of the Internal
                Revenue Code, an organization must be
                [organized](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Organizational-Test-Internal-Revenue-Code-Section-501%28c%29%283%29)
                and
                [operated](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Operational-Test-Internal-Revenue-Code-Section-501%28c%29%283%29)
                exclusively for [exempt
                purposes](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Exempt-Purposes-Internal-Revenue-Code-Section-501%28c%29%283%29)
                set forth in section 501(c)(3), and none of its earnings
                may
                [inure](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Inurement-Private-Benefit-Charitable-Organizations)
                to any private shareholder or individual. In addition,
                it may not be an [*action
                organization*](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Political-and-Lobbying-Activities)*,
                i.e.,* it may not attempt to influence legislation as a
                substantial part of its activities and it may not
                participate in any campaign activity for or against
                political candidates.

            2.  Organizations described in section 501(c)(3) are
                commonly referred to as *charitable organizations*.
                Organizations described in section 501(c)(3), other than
                testing for public safety organizations, are eligible to
                receive tax-deductible
                [contributions](http://www.irs.gov/Charities-&-Non-Profits/Substantiating-Charitable-Contributions)
                in accordance with Code section 170.

            3.  The organization must not be organized or operated for
                the benefit of [private
                interests](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Inurement-Private-Benefit-Charitable-Organizations),
                and no part of a section 501(c)(3) organization's net
                earnings may inure to the benefit of any private
                shareholder or individual. If the organization engages
                in an [excess benefit
                transaction](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Intermediate-Sanctions-Excess-Benefit-Transactions)
                with a person having substantial influence over the
                organization, an [excise
                tax](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Intermediate-Sanctions)
                may be imposed on the person and any organization
                managers agreeing to the transaction.

            4.  Section 501(c)(3) organizations are restricted in how
                much political and legislative (*lobbying*) activities
                they may conduct. For a detailed discussion, see
                [Political and Lobbying
                Activities](http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Political-and-Lobbying-Activities).
                For more information about lobbying activities by
                charities, see the article [Lobbying
                Issues](http://www.irs.gov/file_source/pub/irs-tege/eotopicp97.pdf);
                for more information about political activities of
                charities, see the FY-2002 CPE topic [Election Year
                Issues](http://www.irs.gov/file_source/pub/irs-tege/eotopici02.pdf).

            5.  To Apply as 501(c)3: fill out form 1023
                <http://www.irs.gov/pub/irs-pdf/i1023ez.pdf>

            6.  http://www.stayexempt.irs.gov/Starting-Out/Interactive-Form-1023-Prerequisite-Questions

10. **Merchandise:** Clubs have a variety of merchandise that they sell
    throughout the year at events. Once a club is established, they may
    maintain an inventory of merchandise. In the early stages, however,
    clubs may opt to use a pre-pay option for merchandise.

    a.  Possible merchandise ideas: t-shirts, sweatshirts, koozies,
        water bottles, hats, bumper stickers, window decals

    b.  Potential online vendors: VistaPrint, CustomInk, TeeSpring,
        Vizzle

11. **Bylaws:** Each DockDogs Affiliate Club will use the club bylaws
    template developed by the Rules Committee. There are several built
    in options by which each club may personalize their individual
    bylaws while still remaining true to the Values and Mission
    Statement of DockDogs Worldwide. A copy of this template may be
    found in Appendix A.

> ***Rules, Policies, & Procedures***

1.  **Rules Committee**

    a.  **Meeting Schedule**: Rules Committee meets annually to review
        the current DockDogs Rules, Policies, Procedures. Amendments to
        these Rules are discussed and voted on by the current Rules
        Committee members. Updated DockDogs Rules, Policies, Procedures
        are released in January of each year.

    b.  **Selecting attendees:** Each club is encouraged to send 1-2
        members to represent their club at the annual Rules
        Committee meeting. These representatives should be members in
        good standing, possibly Governing Board members, and
        knowledgeable in the current Rules, Policies, and Procedures of
        DockDogs Worldwide. Attending Rules Committee is a good
        opportunity to develop future/new Governing Board members.

    c.  **Roles of Rules Committee members**: Rules Committee members
        must be able to consider the DDWW Rules from the perspective of
        then sport as a whole and not focus on how they impact the
        individual personally. The purpose of Rules Committee is to grow
        the sport and ensure equitable treatment for all competitors. In
        addition to the annual Rules Committee meeting, members will be
        required to participate in periodic committee conference calls
        as the need arises.

    d.  **Confidentiality of proceedings at Rules**: Each member of the
        DockDogs Rules Committee will be required to sign a
        Nondisclosure Agreement. Information shared during the annual
        meeting and subsequent conference calls are confidential in
        nature and are not to be discussed with individuals who are not
        also members of the Rules Committee.

2.  **Current Rules, Policies, Procedures**: A copy of the most recent
    Rules, Policies, and Procedures can be found in Appendix B

> ***Practices***

1.  Finding a location

2.  Frequency/duration

3.  Committee to organize

4.  Insurance requirements

> ***Events***

1.  Gaining events

    a.  How to find events

<!-- -->

i.  Check with your local Chamber of Commerece

ii. Search the Web for Fairs and Festivals in your State

iii. Approach local sporting goods stores (Bass Pro, Cabela’s ect.)

iv. Secure a venue, enough sponsors and vendors to host your own event
    CAUTION will need to secure a minimum amount of \$5000.00

    a.  Pricing structure

<!-- -->

a.  Event Budget

    -   Rig Rental

    -   Staff

    -   Awards

    -   Insurance

    -   Prizing

    -   Printer Ink and Paper to print Club on site forms

    -   Judges if needed (travel / hotel)

    -   Announcer if needed ( travel /hotel) See Appendix C

<!-- -->

1.  **Pre-event**

    a.  **Reserving rig with DDWW**: When you are first approached about
        a potential event, your first step is to determine if a
        pool/dock is available for that date. To do this, you contact
        DockDogs via the Rig Rental Availability Form. This form is \#2
        on the Affiliate Club Resource Center. It is a Google.doc form
        labeled “Submit Event Date Availability Request (Rig Rental).
        Once you submit this form, you should get a response within a
        few days on the availability of a pool/dock on your
        specific date. If you do not receive a response, a backup method
        for checking availability is to email
        <brian.sharenow@dockdogs.com> with specifics of your event with
        location and dates.

    b.  **Contract:** A template of event contract is available on the
        Affiliate Club Resource Center at
        [www.dockdogs.com](http://www.dockdogs.com) Click on the file
        “event forms” and then “pre-event forms.” The template is
        labeled ClubEventContract-Template

    c.  **Insurance:** Per the DockDogs Affiliate Club Application &
        Contractual Agreement, all clubs must use Frazier Insurance
        Company for their insurance needs for both practices and events.
        You can find the fillable form on the Affiliate Club
        Resource center. Click on the file “event forms” and then
        “pre-event forms.” The form is labeled DockDogs Sanctioned Event
        Insurance Enrollment Form Fillable. An example of a completed
        form can be found in Appendix C.

    d.  **Determining wave schedules for event**: When setting the
        schedule of qualifying waves at an event, it is recommended to
        consider the following factors

        i.  Schedule of activities at overall event (concerts, different
            competitive events, demonstrations, etc.)

        ii. Allow sufficient time between waves based on projected
            attendance

        iii. Consider time of day when scheduling Extreme Vertical

        iv. Consider the time required for Admin to process wave results
            when scheduling Finals

    e.  **Promotion**: Use all available avenues to promote your event.
        Social media is an excellent tool, but don’t forget to contact
        your local radio/television stations. They may have time in
        their programming for local stories which could highlight your
        club and your upcoming event.

    f.  **Inspecting a potential site**: These are the site features you
        should consider when doing your initial site visit.

        i.  Pool site must be level within six (6) inches over a 45’X25’
            area and should face East/West

        ii. Need 100’X100’ for the competition area this includes “Dog
            Town” and Admin. Tent

        iii. Access to electric for admin or generator required

        iv. Area for spectators and bleachers

        v.  Water access for filling the pool

        vi. Drainage access for the pool

        vii. Restroom access, space for Port-a-John

        viii. Competitor parking, access to drop off and load and
            parking fees

        ix. Competitor set up times

        x.  Admission fees for Competitors and accompanying party

        xi. Parking for the Club trailer

        xii. Venue surface – Grass or Pavement

        xiii. Are competitor generators and tent staking permitted

        xiv. Slip hazards

        xv. Food on site, are coolers permitted, alcohol policy

        xvi. Camping on site, RVs permitted

        xvii. Over night security

        xviii.  Check if any permits are required

    g.  **Determining prize structure**: There are several variations of
        prizing structures within the different DockDogs
        Affiliate Clubs.

        i.  Ribbons/medals for each qualifying wave

        ii. Ribbons/medals for Big Air finals, Speed Retrieve Finals,
            Extreme Vertical Finals, Iron Dog

        iii. Prize purse: not all clubs provide a cash payout but those
            that do usually mirror the cash payout utilized by DDWW at
            national events, however, that is the Club’s option

    h.  **Determining Finals:** The Club has the option to choose how
        the Event Finals are run. Examples in Appendix D

    i.  The National 4 Tier Finals

        **ii.** Divisional Finals which can include Youth Handler, Lap
        Dog, Veteran Dog and Legend Dog

    j.  **Ordering medals/ribbons**: DockDogs worldwide has a contract
        with Crown Awards for their medals. The DockDogs® Store is
        located at
        [*https://www.crownawards.com/StoreFront/DDG.DockDogs\_Trophies\_And\_Awards.cat*](https://www.crownawards.com/StoreFront/DDG.DockDogs_Trophies_And_Awards.cat).
        Our contact person with Crown Awards is Rachel Williams
        <rwilliams@crownawards.com> or she can be reached at
        1-800-542-6044 ext. 613. It is recommended to not order Event
        Specific Awards.

    k.  **Finding sponsors:** Hopefully your event host will have all
        the required sponsors for the event. However, if you are in a
        position to help them find sponsor the event, there are
        sponsorship structures available. See appendix D

    l.  **Organizing volunteers**: There are several tools, which clubs
        use to help them organize volunteers. Some make an Excel
        spreadsheet but others use the website SignUpGenius.com. There
        is no one specific tool that must be used, but the more
        organized you are on the front end, the more successful your
        event will be.

    m.  **Certified judges:** Your DockDogs driver will be a certified
        judge but your club must provide the other judges. Per the
        DockDogs Rules/Policies, one judge must remain consistent
        throughout the event. If you do not have any certified judges
        within you club, reach out to your neighboring clubs for judges
        who might assist with your event on a volunteer basis. Some
        clubs hire certified judges to come to their event.

    n.  **Announcer notebook:** Developing a notebook for your
        announcers will improve the overall impression of your event.
        The notebook can contain information about the event, your host,
        and your competitors. For Announcer sheet see Appendix E

2.  **Weekend of event**

    a.  **Set up:** You will need at least 6-8 club members to help the
        DockDogs staff to set up the pool and dock as well as
        club areas. It is best if at least 2-3 are capable of helping
        with the heavy lifting.

    b.  **Tear down:** You will once again need 6-8 club members to help
        the DockDogs staff to drain the pool and pack everything back
        into the trailer. It is best if at least 2-3 are capable of
        helping with the heavy lifting.

    c.  **Pool requirements:** You will need to work with your event
        host to get the pool filled with 26,000 gallons of water.. Clubs
        had frequently been successful in having their local fire
        department help with the filling of the pool.

    d.  **Communication with DDWW Administrative Staff:** Communication
        with your DockDogs staff is crucial for a successful event. They
        are your link to your competitors and they will help the event
        run smoothly and on schedule. When reviewing your expectations
        for the running of the event, make sure to address these key
        issues:

        i.  schedule of the event

        ii. special circumstances related to the event

        iii. format for finals

        iv. any specialty finals or showcases planned for the event

        v.  If your club uses fee waivers/vouchers for volunteers,
            review how those are to be cashed in (\$ value, used for
            entire wave fee, limit on amount per wave, etc)

    e.  **Work schedule for volunteers**: Your event will run more
        smoothly with volunteer positions filled prior to the start of
        the event. In the event that you haven’t been able to get all
        positions filled, designate one person to wrangle volunteers at
        the start of the event. You will need the following positions:

        i.  Big Air Waves

            1.  Practice wrangler/coach

            2.  Wave wrangler

            3.  Scorekeeper

            4.  2 Certified Judges (one should be the DDWW staff)

            5.  Announcer

            6.  Administration Assistant

        ii. Speed Retrieve Waves

            1.  Practice coach/wrangler

            2.  Wave wrangler

            3.  Scorekeeper (averages the two times for final score)

            4.  2 timers

            5.  bumper runner

            6.  bumper hanger

            7.  line judge (must be certified judge, typically
                DDWW staff)

            8.  announcer

            9.  Administration Assistant

        iii. Extreme Vertical Wave

            1.  Practice coach/wrangler

            2.  Wave wrangler

            3.  Scorekeeper

            4.  Announcer

            5.  Administration Assistant

            6.  EV assistant (help DDWW staff hang bumper quickly to
                keep competition flowing smoothly)

    f.  **Materials Necessary for event**: The following materials are
        necessary for a successful event.

        i.  Ream of legal size paper (8.5”x14”)

        ii. Wristbands
            ([www.24hourwristbands.com](http://www.24hourwristbands.com))

        iii. Pens (at least 10)

        iv. Highlighters (multiple colors)

        v.  Stapler & staples

        vi. Rubber bands

        vii. Scotch tape

        viii. Duct tape

        ix. Caution tape

        x.  Clipboards (at least5-7, legal size clipboards are best)

        xi. Surge protector power bar

        xii. I Phone or other means of music along with cords See
            Appendix F for suggested Playlist

        xiii. Onsite forms

            1.  registration forms (template is available on the
                Resource Center)

            2.  Adult and Minor Waiver Forms

            3.  Incident Report Form

            4.  Flash drive with all event forms

        xiv. Wave/finals awards

        xv. Two stop watches which go out three decimal points (0.000)

        xvi. Poop bags

        xvii. Multiple sizes of Garbage Bags

    g.  Optional Items: These items are useful and will aid the running
        of the event, but are not necessary.

        i.  Folding 6’ tables

        ii. Folding chairs (hard seat)

        iii. Generator

        iv. Tents to shade the competitors in line

        v.  Tent weights (buckets w/water, sand bags, etc.)

        vi. Computer chilly pads

        vii. fans

    h.  Merchandise

    i.  Relationship with event host

    j.  First aid kit See Appendix G

    k.  Rules infractions

3.  Post-event

    a.  Follow up with event host

    b.  Complete the mandatory Post Event Report at
        *http://dockdogs.com/private-affiliate-club-resource-center/*

    c.  Follow up with club members

> ***Club meetings***

1.  Business meetings

2.  Social events

> ***Miscellaneous Club Activities***

1.  Voucher system

2.  Community Outreach events


